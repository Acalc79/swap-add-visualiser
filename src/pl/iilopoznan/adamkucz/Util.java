package pl.iilopoznan.adamkucz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Util {
    private static final Scanner sc= new Scanner(System.in);

    public static String[] readLines(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        ArrayList<String> lines = new ArrayList<>();
        while (true) {
            String line = reader.readLine();
            if (line == null) break;
            lines.add(line);
        }
        return lines.toArray(new String[0]);
    }

    public static ProgramLine[] readProgram(File programFile) throws IOException {
        return parseLines(readLines(programFile));
    }

    public static ProgramLine[] parseLines(String[] lines) {
        HashMap<Label,Integer> labelMap = new HashMap<>();
        for (int i = 0; i < lines.length; ++i) {
            String line = lines[i];
            if (ProgramLine.isLineLabel(line)) {
                Label label = new Label(line);
                if (labelMap.containsKey(label)) {
                    throw new IllegalArgumentException();
                }
                labelMap.put(label, i);
            }
        }
        return Arrays.stream(lines)
                .map(line -> ProgramLine.isLineLabel(line) ?
                        new Label(line) : new Instruction(line, labelMap))
                .toArray(ProgramLine[]::new);
    }

    public static boolean isAllUppercase(String s) {
        for(char c : s.toCharArray()) {
            if(Character.isLetter(c) && Character.isLowerCase(c)) {
                return false;
            }
        }
        return true;
    }

    public static int getInt() {
        System.out.print("User input needed: ");
        return sc.nextInt();
    }
}
