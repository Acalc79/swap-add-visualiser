package pl.iilopoznan.adamkucz;

import java.util.EnumSet;
import java.util.Map;

public class Instruction extends ProgramLine {
    public enum Name {
        INPUT, OUTPUT, LOAD, STORE, SWAP, ADD, ADDI, JUMP, JUMPZ;

        public static EnumSet<Name> noArgs() {
            return EnumSet.of(INPUT, OUTPUT, SWAP, ADD);
        }
        public static EnumSet<Name> oneArg() {
            return EnumSet.complementOf(noArgs());
        }
        public int numArgs() {
            return noArgs().contains(this) ? 0 : 1;
        }
        public boolean isJump() {
            return this == JUMP || this == JUMPZ;
        }
        public boolean isMemOp() {
            return this == LOAD || this == STORE;
        }
    }

    private final Name name;
    private Integer argument;

    public Name getName() {
        return name;
    }

    public Integer getArgument() {
        return argument;
    }

    public Instruction(String line, Map<Label, Integer> labelMap) {
        String[] elements = line.split("\\s+");
        name = Name.valueOf(elements[0].toUpperCase());
        if (elements.length != 1 + name.numArgs()) {
            throw new IllegalArgumentException();
        }
        if (name.isJump()) {
            argument = labelMap.get(new Label(elements[1]));
        }
        else if (name.numArgs() == 1) {
            argument = Integer.valueOf(elements[1]);
            if (name.isMemOp() && (argument < 0 || argument >= 64)) {
                throw new IllegalArgumentException();
            }
        }
    }
}
