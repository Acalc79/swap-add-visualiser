package pl.iilopoznan.adamkucz;

// source code
public class ProgramLine {
    public static boolean isLineLabel(String line) {
        return Character.isUpperCase(line.charAt(0));
    }
}