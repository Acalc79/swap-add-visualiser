package pl.iilopoznan.adamkucz;

import java.util.Arrays;
import java.util.HashMap;

public class Memory {
    private final int[] array;

    public Memory() {
        array = new int[64];
    }

    public void storeAt(int addr, int val) {
        if (addr < 0 || addr >= 64) {
            throw new IllegalArgumentException();
        }
        array[addr] = val;
    }

    public int loadFrom(int addr) {
        if (addr < 0 || addr >= 64) {
            throw new IllegalArgumentException();
        }
        return array[addr];
    }

    public int occupancy() {
        return (int) Arrays.stream(array).filter(i -> i != 0).count();
    }

    @Override
    public String toString() {
        if (occupancy() > 20) {
            return "Memory" + Arrays.toString(array);
        }
        else {
            HashMap<Integer, Integer> sparse = new HashMap<>();
            for (int i = 0; i < array.length; ++i) {
                if (array[i] != 0) {
                    sparse.put(i, array[i]);
                }
            }
            return "Memory" + sparse;
        }
    }
}
