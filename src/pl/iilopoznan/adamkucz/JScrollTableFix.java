package pl.iilopoznan.adamkucz;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class JScrollTableFix implements ComponentListener {
    private final JTable table;
    private final JScrollPane pane;
    private final int rowSize;

    public JScrollTableFix(JTable table, JScrollPane pane, int rowSize) {
        assert table != null;
        this.table = table;
        this.pane = pane;
        this.rowSize = rowSize;
    }

    @Override
    public void componentResized(final ComponentEvent event) {
        Component comp = event.getComponent();
        // turn off resize and let the scroll bar appear once the component is smaller than the table
        if (comp.getWidth() < table.getPreferredSize().getWidth()) {
            pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        }
        // otherwise resize new columns in the table
        else {
            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        }

        int availableRowHeight = comp.getHeight() / table.getRowCount();
        if (availableRowHeight > rowSize) {
            table.setRowHeight(availableRowHeight);
            pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        }
        else {
            table.setRowHeight(rowSize);
            pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {}
    @Override
    public void componentShown(ComponentEvent e) {}
    @Override
    public void componentHidden(ComponentEvent e) {}
}
