package pl.iilopoznan.adamkucz;

import java.io.File;
import java.io.IOException;

public class SwapAddDemo {
    public static void main(String[] args) throws IOException {
        String[] programLines = Util.readLines(new File(args[0]));
        ProgramLine[] program = Util.parseLines(programLines);
        CPU cpu = new CPU(program, Util::getInt, System.out::println);
//        cpu.run();
//        System.out.println(cpu);
//        for (var i = 0; i < 20; ++i) {
//            cpu.step();
//            System.out.println(cpu);
//        }
        new View(programLines, cpu);
    }
}