package pl.iilopoznan.adamkucz;

import java.awt.event.ActionEvent;

public class Controller {
    private final CPU cpu;
    private final Runnable runOnChange;

    public Controller(CPU cpu, Runnable onChange) {
        this.cpu = cpu;
        runOnChange = onChange;
    }

    public void doRun(ActionEvent e) {
        while (!cpu.finished()) {
            doStep(e);
        }
    }

    public void doStep(ActionEvent e) {
        cpu.step();
        runOnChange.run();
    }
}
