package pl.iilopoznan.adamkucz;

import java.util.Objects;

public class Label extends ProgramLine {
    private final String name;

    public String getName() {
        return name;
    }

    public Label(String labelName) {
        name = labelName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Label label = (Label) o;
        return name.equals(label.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
