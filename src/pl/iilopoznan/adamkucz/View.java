package pl.iilopoznan.adamkucz;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.stream.IntStream;

public class View {
    private static final int CELL_SIZE = 40;
    private final JFrame frame;

    public View(String[] programLines, CPU cpu) {
        Font defaultFont = new Font ("Arial", Font.PLAIN, 30);
        UIDefaults defaults = UIManager.getLookAndFeelDefaults();
        defaults.put("Button.font", defaultFont);
        defaults.put("List.font", defaultFont);
        defaults.put("Table.font", defaultFont);
        defaults.put("Label.font", defaultFont);
        defaults.put("OptionPane.font", defaultFont);
        defaults.put("TextField.font", defaultFont);

        frame = new JFrame();
        Container pane = frame.getContentPane();
        GridBagLayout gridBag = new GridBagLayout();
        pane.setLayout(gridBag);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.ipadx = 5;
        c.ipady = 5;
        c.insets = new Insets(5,5,5,5);
        c.gridy = 0;
        c.weighty = 1;

        JPanel leftPanel = new JPanel();
        c.gridx = 0;
        c.gridwidth = 1;
        c.weightx = 1;
        gridBag.setConstraints(leftPanel, c);
        pane.add(leftPanel);

        JPanel centrePanel = new JPanel();
        c.gridx = 1;
        c.gridwidth = 2;
        c.weightx = 2;
        gridBag.setConstraints(centrePanel, c);
        pane.add(centrePanel);

        JPanel rightPanel = new JPanel();
        c.gridx = 3;
        c.gridwidth = 3;
        c.weightx = 3;
        gridBag.setConstraints(rightPanel, c);
        pane.add(rightPanel);

        leftPanel.setLayout(new BorderLayout());
        JList<String> programList = new JList<>(programLines);
        programList.setBounds(10, CELL_SIZE/2, CELL_SIZE*7/2, CELL_SIZE*10);
        programList.setSelectedIndex(cpu.getPc());
        JScrollPane programPane = new JScrollPane(programList);
        programPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        programPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        leftPanel.add(programPane);

        GridBagLayout gridBagRight = new GridBagLayout();
        rightPanel.setLayout(gridBagRight);
        c.fill = GridBagConstraints.BOTH;
        c.ipadx = 0;
        c.ipady = 5;
        c.insets = new Insets(5,5,5,5);
        c.gridy = 0;
        c.weighty = 1;

        JTable tableLegend = new JTable(
                IntStream.range(0,8).mapToObj(i -> new Integer[]{i * 8}).toArray(Integer[][]::new),
                new String[]{""});
        DefaultTableCellRenderer centreRenderer = new DefaultTableCellRenderer();
        centreRenderer.setHorizontalAlignment(JLabel.RIGHT);
        centreRenderer.setVerticalAlignment(JLabel.CENTER);
        tableLegend.setDefaultRenderer(Object.class, centreRenderer);

        JScrollPane tableLegendPane = new JScrollPane(tableLegend);
        tableLegendPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        tableLegendPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        tableLegendPane.addComponentListener(new JScrollTableFix(tableLegend, tableLegendPane, CELL_SIZE));

        c.gridx = 0;
        c.gridwidth = 1;
        c.weightx = 1;
        gridBagRight.setConstraints(tableLegendPane, c);
        rightPanel.add(tableLegendPane);

        AbstractTableModel memTableModel = new AbstractTableModel() {
            @Override public int getRowCount() { return 8; }
            @Override public int getColumnCount() { return 8; }
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return cpu.getMem().loadFrom(rowIndex * 8 + columnIndex);
            }
        };
        JTable memTable = new JTable(memTableModel);
        centreRenderer.setHorizontalAlignment(JLabel.CENTER);
        memTable.setDefaultRenderer(Object.class, centreRenderer);
        //memTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        memTable.setMinimumSize(new Dimension(CELL_SIZE * 8, CELL_SIZE * 8));
        memTable.setTableHeader(null);

        JScrollPane tablePane = new JScrollPane(memTable);
        tablePane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        tablePane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        tablePane.addComponentListener(new JScrollTableFix(memTable, tablePane, CELL_SIZE));

        c.gridx = 1;
        c.gridwidth = 8;
        c.weightx = 8;
        gridBagRight.setConstraints(tablePane, c);
        rightPanel.add(tablePane);

        GridBagLayout gridBagCentre = new GridBagLayout();
        centrePanel.setLayout(gridBagCentre);
        c.ipadx = 20;
        c.ipady = 20;
        c.insets = new Insets(20,20,20,20);

        JButton stepButton = new JButton("Step");
        c.gridx = 0;
        c.gridwidth = 1;
        c.weightx = 1;
        c.gridy = 5;
        c.weighty = 1;
        gridBagCentre.setConstraints(stepButton, c);
        centrePanel.add(stepButton);

        JButton runButton = new JButton("Run");
        c.gridx = 1;
        gridBagCentre.setConstraints(runButton, c);
        centrePanel.add(runButton);

        JLabel acc = new JLabel("ACC: " + cpu.getAcc());
        acc.setHorizontalAlignment(JLabel.CENTER);
        c.gridx = 0;
        c.gridwidth = 2;
        c.weightx = 1;
        c.gridy = 2;
        c.weighty = 1;
        gridBagCentre.setConstraints(acc, c);
        centrePanel.add(acc);

        JLabel mdr = new JLabel("MDR: " + cpu.getMdr());
        mdr.setHorizontalAlignment(JLabel.CENTER);
        c.gridy = 3;
        gridBagCentre.setConstraints(mdr, c);
        centrePanel.add(mdr);

        JLabel finished = new JLabel("FINISHED!");
        finished.setHorizontalAlignment(JLabel.CENTER);
        finished.setVisible(false);
        c.gridy = 4;
        gridBagCentre.setConstraints(finished, c);
        centrePanel.add(finished);

        DefaultListModel<Integer> inputModel = new DefaultListModel<>();
        JList<Integer> inputList = new JList<>(inputModel);
        cpu.setInput(() -> { int i = getInput(); inputModel.addElement(i); return i; });
        JScrollPane inputPane = new JScrollPane(inputList);
        inputPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        inputPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        JLabel inputLabel = new JLabel("Inputs");
        inputLabel.setHorizontalAlignment(JLabel.CENTER);
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BorderLayout());
        inputPanel.add(inputLabel, BorderLayout.NORTH);
        inputPanel.add(inputPane, BorderLayout.CENTER);
        c.gridx = 0;
        c.gridwidth = 1;
        c.weightx = 1;
        c.gridy = 0;
        c.gridheight = 1;
        c.weighty = 3;
        gridBagCentre.setConstraints(inputPanel, c);
        centrePanel.add(inputPanel);

        DefaultListModel<Integer> outputModel = new DefaultListModel<>();
        JList<Integer> outputList = new JList<>(outputModel);
        cpu.setOutput(outputModel::addElement);
        JScrollPane outputPane = new JScrollPane(outputList);
        outputPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        outputPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        JLabel outputLabel = new JLabel("Outputs");
        outputLabel.setHorizontalAlignment(JLabel.CENTER);
        JPanel outputPanel = new JPanel();
        outputPanel.setLayout(new BorderLayout());
        outputPanel.add(outputLabel, BorderLayout.NORTH);
        outputPanel.add(outputPane, BorderLayout.CENTER);
        c.gridx = 1;
        gridBagCentre.setConstraints(outputPanel, c);
        centrePanel.add(outputPanel);

        Controller controller = new Controller(cpu, () -> {
            acc.setText("ACC: " + cpu.getAcc());
            mdr.setText("MDR: " + cpu.getMdr());
            finished.setVisible(cpu.finished());
            memTableModel.fireTableDataChanged();
            programList.setSelectedIndex(cpu.getPc());
        });
        stepButton.addActionListener(controller::doStep);
        runButton.addActionListener(controller::doRun);

        frame.setSize(800,600);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private int getInput() {
        while (true) {
            String input = JOptionPane.showInputDialog(frame,
                    "The CPU requires integer input",
                    "Input",
                    JOptionPane.INFORMATION_MESSAGE);
            try {
                return Integer.parseInt(input);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(frame,
                        "Invalid input, integer is required!",
                        "Invalid input",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
