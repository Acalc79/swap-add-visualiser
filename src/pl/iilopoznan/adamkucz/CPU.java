package pl.iilopoznan.adamkucz;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public class CPU {
    private final ProgramLine[] program;
    private final Memory mem;
    private IntSupplier input;
    private IntConsumer output;
    private int acc;
    private int mdr;
    private int pc;

    public CPU(ProgramLine[] programLines, IntSupplier input, IntConsumer output) {
        this.input = input;
        this.output = output;
        acc = 0;
        mdr = 0;
        pc = 0;
        program = programLines;
        mem = new Memory();
    }

    public void execute(Instruction instruction) {
        ++pc;
        switch (instruction.getName()) {
            case INPUT:
                acc = input.getAsInt();
                break;
            case OUTPUT:
                output.accept(acc);
                break;
            case LOAD:
                mdr = getMem().loadFrom(instruction.getArgument());
                break;
            case STORE:
                getMem().storeAt(instruction.getArgument(), mdr);
                break;
            case SWAP:
                int temp = acc;
                acc = mdr;
                mdr = temp;
                break;
            case ADD:
                acc += mdr;
                break;
            case ADDI:
                acc += instruction.getArgument();
                break;
            case JUMP:
                pc = instruction.getArgument();
                break;
            case JUMPZ:
                if (acc == 0)
                    pc = instruction.getArgument();
                break;
        }
    }

    public void step() {
        while (pc < program.length) {
            if (program[pc] instanceof Label) {
                ++pc;
            }
            else {
                execute((Instruction) program[pc]);
                break;
            }
        }
    }

    public void run() {
        run(false);
    }

    public void run(boolean display) {
        while (!finished()) {
            step();
            if (display) {
                System.out.println(this);
            }
        }
    }

    public int getAcc() {
        return acc;
    }

    public int getMdr() {
        return mdr;
    }

    public int getPc() {
        return pc;
    }

    public Memory getMem() {
        return mem;
    }

    public boolean finished() {
        return pc >= program.length;
    }

    public void setInput(IntSupplier input) {
        this.input = input;
    }

    public void setOutput(IntConsumer output) {
        this.output = output;
    }

    @Override
    public String toString() {
        return "CPU{" +
                "acc=" + acc +
                ", mdr=" + mdr +
                ", pc=" + pc +
                ", mem=" + mem +
                '}';
    }
}
